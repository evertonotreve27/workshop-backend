from django.urls import path
from .views import PessoaListView, PessoaCreateView, PessoaDeleteView


#CAMINHO DA URL (PAGINA/SITE A IR) RELACIONADAS AS VIEWS

# 127.0.0.1:8000/pessoas
# 127.0.0.1:8000/pessoas/[identificação unica da pessoa -ID]/deletar

#PK -> Primary Key -> ID Identificador do


urlpatterns = [
    #path('', listar_pessoas),
    path('', PessoaListView.as_view(), name='pessoas_pessoa_list'),
    path('criar/', PessoaCreateView.as_view(),  name='pessoas_pessoa_create'),
    path('<int:pk>/delete', PessoaDeleteView.as_view(),  name='pessoas_pessoa_delete')
]

