from django import forms
from .models import Pessoa

class PessoaForm(forms.ModelForm):
    class Meta:
        model = Pessoa
        fields = '__all__' # ['nome','idade']

    def clean_idade(self):
        idade = self.cleaned_data['idade']
        if int(idade) < 0:
            raise forms.ValidationError('Idade não pode ser menor que zero')
        return idade

