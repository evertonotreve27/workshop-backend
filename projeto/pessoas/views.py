#from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, DeleteView
from django.urls import reverse_lazy
from .models import Pessoa
from .forms import PessoaForm

# Local onde é a ponte entre os modelos(banco de dados) se comunicará com os templates (Pagina, Web),
#Podendo haver logica como validanção dos dados. Para inicar o server, utilizar:
#python manage.py runserver


class PessoaListView(ListView):
    model = Pessoa
    template_name = 'pessoas/listar_pessoas.html'
    context_object_name = 'pessoas'

class PessoaCreateView(CreateView):
    model = Pessoa
    template_name = 'pessoas/criar_pessoa.html'
    form_class = PessoaForm
    success_url = reverse_lazy('pessoas_pessoa_list')

class PessoaDeleteView(DeleteView):
    model = Pessoa
    success_url = reverse_lazy('pessoas_pessoa_list')



#def listar_pessoas(request):
#    pessoas = Pessoa.objects.all()
#    return render(request, 'pessoas/listar_pessoas.html', { 'pessoas': pessoas })

##def criar_pessoa(request):
##    if request.method == 'GET':
##       return render(request, 'pessoas/criar_pessoa.html')
##    else: #POST
##       nome = request.POST['nome']
##      idade = int(request.POST['idade'])
##      if idade < 0:
##          return render(request, 'pessoas/criar_pessoa.html',{'erro': 'Idade não pode ser menor que zero'})
##      pessoa = Pessoa(nome=nome, idade=idade)
##      pessoa.save()
##      return redirect('/pessoas')